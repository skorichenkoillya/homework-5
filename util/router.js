

class Router {
  constructor(server, basePath) {
    if (!basePath.startsWith('/')) {
      basePath = '/' + basePath;
    }
    this.basePath = basePath;
    this.server = server;
  }


  static HTTP_METHODS = {
    GET: 'GET',
    POST: 'POST',
    PATCH: 'PATCH',
    PUT: 'PUT',
    DELETE: 'DELETE',
    OPTIONS: 'OPTIONS'
  };

  /**
   *
   * @param {string} url
   * @return {boolean}
   */
  #baseCheck(url) {
    return url.startsWith(this.basePath)
  }

  #checkSubRoute(subRoute, url) {
    return url.startsWith(subRoute, this.basePath.length + 1);
  }

  #makeParams(subRoute, url){
    const fullRoute = `${this.basePath}/${subRoute}`;
    const params = fullRoute.split('/').reduce((accumulator, param, index) => {
      if (param.match(/{\w+}/)) {
        const key = param.replace(/[{}]/g, '');
        return Object.assign(accumulator, {
          [key]: {
            value: undefined,
            index
          }
        })
      }
      return accumulator;
    }, {});

    url.split('/').map((value, index) => {
      Object.keys(params).map((key) => {
        if (params[key].index === index){
          params[key] = value
        }
      });
    })

    return params;
  }

  get(subRoute, getHandler) {
    if (subRoute.startsWith('/')) {
      subRoute = subRoute.slice(1);
    }
    this.server.on('request', (req, res) => {
      if(!this.#baseCheck(req.url)) {
        return;
      }
      if (this.#checkSubRoute(subRoute, req.url) && req.method === Router.HTTP_METHODS.GET) {
        res.taken = true;

        getHandler(req, res);
      }
    })
  }

  delete(subRoute, getHandler) {
    if (subRoute.startsWith('/')) {
      subRoute = subRoute.slice(1);
    }
    this.server.on('request', (req, res) => {
      if(!this.#baseCheck(req.url)) {
        return;
      }
      if (req.method === Router.HTTP_METHODS.DELETE) {
        res.taken = true;
        req.params = this.#makeParams(subRoute ,req.url);
        getHandler(req, res);
      }
    })
  }

  post(subRoute, postHandler) {
    if (subRoute.startsWith('/')) {
      subRoute = subRoute.slice(1);
    }
    this.server.on('request', (req, res) => {
      if(!this.#baseCheck(req.url)) {
        return;
      }
      if (this.#checkSubRoute(subRoute, req.url) && req.method === Router.HTTP_METHODS.POST) {
        res.taken = true;

        let body = "";
        req.on('data', (chunk) => {
          body += chunk;
        });

        req.on('end', () => {
          req.body = body;

          postHandler(req, res);
        });

      }
    })
  }
}

module.exports = Router;
